# Policy Technology

## Decision

We should use OPA ([Open Policy Agent](https://www.openpolicyagent.org/)) to implement this side car project.

## Rationale

OPA become a standard to implement Policy Engine and Rego to describe rules.

Rules provider will be able to sign rules bundle to enforce confident and be sure of the identity of the rule provider.

While Typescript is a good way to easily and quickly push a concept, OPA seems more aligned with federated approach of heterogeneous rules implementation for a federation use.

## Approach

- V1. Continue integration of abc-checker thanks to Rule #1 developed with Typescript.
- V2. When integration is finished, migrate from Typescript to OPA for Rule #1 and future rules. 
    - Re-use code from https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service and https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/policy-rules-provider
    - Optional: Consider the ability to choose between either "hard coded typescript rules" or "Rego rules through the OPA engine (internal or external)" at initialisation startup
