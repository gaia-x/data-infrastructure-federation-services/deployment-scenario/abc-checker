package gaiax.core.abc.checker

import data.gaiax.lib.utils
import data.gaiax.lib.vc.vcservice
import data.gaiax.lib.vp.vpservice

import future.keywords.if
import future.keywords.in
import future.keywords.every


default rule_01 = false
default rule_02 = false
default rule_03 = false
default rule_04 = false
default rule_05 = false
default rule_06 = false
default rule_07 = false
default rule_08 = false
default rule_09 = false
default rule_10 = false
default rule_11 = false
default rule_12 = false
default rule_13 = false
default rule_14 = false
default rule_15 = false
default rule_16 = false
default rule_17 = false
default rule_18 = false
default rule_19 = false
default rule_20 = false
default rule_21 = false


sign_result := [false, true]
#Status: DONE
#Description: Check if Participant is already in Registry
#WARNING: But need to update the Cert of Participant Dufour
#Test: Use VP. The function iterates over all contained VCs and pick the compliance one to check
rule_01 {
	vpservice.contains_participant_in_registry(input.vp) == true
}

#Status: DONE
#Description: Check if the VC Participant is of type Provider
#Test: Use VP. The function iterate over all contained VCs to check if Participant is a Provider
rule_02 {
	vpservice.contains_valid_provider_vc(input.vp) == true
}

#Status: DONE
#Description: Verify the signatures of all VCs and the VP itself 
#Test: Use VP. The function iterates over the VP itself and all contained VCs to verify
rule_03 := res {
	#vpservice.is_vp_signed(input.vp) == true
	vp := input.vp 
	
	is_vp_signed := [{
		"vc_type" : "vp",
		"is_signed" : vpservice.is_vp_signed(vp) 
	}]
	vcs := vp.verifiableCredential
	
	is_vcs_signed := [is_vc_valid |
		vc := vcs[_]
		vc_id = vc["id"]
		is_issuer_signed := to_number(utils.is_signature_valid(vc))
		is_compliance_signed := to_number(utils.is_compliance_signature_valid(vc))
		is_signed := is_issuer_signed + is_compliance_signed
		is_vc_valid := {
			"vc_id": vc_id,
			"is_signed" : sign_result[is_signed],
			"status" : vcservice.verify_credential_status(vc)
		}
	]
	res := array.concat(is_vp_signed, is_vcs_signed)
}

#Status: DONE
#Description: Check if all VCs in the VP belong to the same Provider
#Test: Use VP. The function iterates over all contained VCs to check if all VCs belong to the same Participant
rule_04 {
	vpservice.contains_vcs_of_same_provider(input.vp) == true
}

#Status: DONE
#Description: Verify if Federated Catalogue and Compliance belongs to the same Federation
#Test: Use VP. The function iterates over all VCs delivered by Compliance and check if it belongs to the same federation as FC
rule_05 {
	vpservice.belong_to_federation(input.vp) == true
}

#Status: DONE
#Description: Verify the integrity of data objects
#Test: Use VP
rule_06 { 
	vpservice.verify_vcs_integrity(input.vp) == true
}


#Description: Check if the T&C contained in the asosciated VC are the same as the federation
rule_07 { 
	vcservice.check_tc_validity(input["aster-conformity:TermsAndConditions"], input["rule_07:registryURL"])
}

#Description: Check if the issuer of the compliance credential is trusted
rule_08 { 
	vcservice.check_trusted_issuer(input["gx:compliance"], input["rule_08:registryURL"])
}

rule_09 { 
	vcservice.check_matching_schema(input["gx:LegalParticipant"], input["rule_09:registryURL"])
}

#Description: Check if the issuer of the compliance credential is trusted and that compliance is that of a service offering
rule_10 { 
	vcservice.check_Federation_Participant(input["vcard:Organization"], input["rule_10:registryURL"])
}

rule_11 { 
	vcservice.check_SO_Trusted(input["gx:compliance"], input["rule_11:registryURL"])
}

#Status: DONE
#Description: Verify the signatures of all VCs (not empacted in VP)
rule_12 := res {

	vc := input.vc
    is_issuer_signed := to_number(utils.is_signature_valid(vc))
    is_compliance_signed := to_number(utils.is_compliance_signature_valid(vc))
    is_signed := is_issuer_signed    + is_compliance_signed
    print(is_signed)
    res := sign_result[is_signed]

}

rule_13 := res {
	vc := input.vc
	res :=  vcservice.verifyExpirationDateExistence(vc)
}

rule_14 := res {
	vc := input.vc
    res := vcservice.verifyExpirationDate(vc)

}
rule_15 := res {

	vc := input.vc
	res := vcservice.verify_credential_status_existence(vc)
}

rule_16 := res {

	vc := input.vc
    res := vcservice.verify_credential_statusValidity(vc)

}

rule_20 := res {

	vc := input["gx:LegalParticipant"]
    res := vcservice.verify_aster_business_field(vc)

}

rule_21 := res {

	vc := input["gx:LegalParticipant"]
    res := vcservice.validate_aster_business_field(vc)

}


#Status: DONE
#Description: Verify the status of a credential
#In case of StatusList, credential is valid if not being revoked or suspended
#In case of BitString, credential is valid if currently assigned to status 0x00. In any other case, its considered as invalid
# Result: false means that credential status is either invalid or can not be verified
# Result: true means that credential status is valid
rule_19 := res {
    vc := input.vc
#     vc := input
    res := vcservice.verify_credential_status(vc)
}