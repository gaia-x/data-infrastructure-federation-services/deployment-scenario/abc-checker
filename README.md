# ABC-Checker
# Build and Test Procedure
## Run ABC-Checker locally with CMD without even starting an OPA Server
To run ABC-Checker with this option, you need to make sure that you have OPA installed on your machine. You do not need to start the OPA server.

To start evaluating rule, you can have two options: Verify single rule one by one or verify a dynamic set of rules

1. VERIFICATION OF SINGLE RULE

With this option, you can only verify ONE single rule at a time. All single rules of ABC-Checker are defined in the <mark>abc-checker.rego</mark> file.

```
  opa eval --data services/utils.rego --data services/vc_service.rego --data services/vp_service.rego --data abc-checker.rego --data main.rego --input "data/vp.json" "data.gaiax.core.abc.checker.rule_01"  
```
Note that in the above command, you MUST include all the modules developed for ABC-Checker with the --data tag, including:
- <mark>utils.rego</mark>
- <mark>vc_service.rego</mark>
- <mark>vp_service.rego</mark>
- <mark>abc-checker.rego</mark>
- <mark>main</mark>

To specify the input data, you MUST use the --input tag, followed by the data itself

To specify the rule to be evaluated, you MUST use the keyword <mark>data</mark> followed by the package name of the file where the rule is defined. In this example, rule_01 is defined in the <mark>abc-checker.rego</mark> whose package name is <mark>gaiax.core.abc.checker</mark>

2. VERIFICATION OF A DYNAMIC SET OF RULES

With this option, you can verify a set of rules at once. The "abc-checker" rule, which is defined inside the <mark>main.rego</mark> rule, serves as an ENTRY point. This rule receive a request, in which User can define a set of single rules to be evaluated. The format of such a request can be found in the data/composable.json. You can find an example which uses this option.
```
opa eval --data services/utils.rego --data services/vc_service.rego --data services/vp_service.rego --data main.rego --data abc-checker.rego --input "data/composable.json" "data.gaiax.core.abc_checker"
```


## Run ACB-Checker locally with OPA Server running

You can also test the ABC-Checker project by first running the OPA Server by following the command below: 
```
opa run --server
```
This command will launch an OPA Server at the port 8181 by default. After this, you need to register all the modules including <mark>utils</mark>, <mark>vc_service</mark>, and <mark>vp_service</mark> as well as <mark>main</mark> module. For this purpose, you need to send the corresponding requests:
### Policy Registration
In order to start evaluating all the rules, we need to first register all the following rules. Refer to the [official documentation](https://www.openpolicyagent.org/docs/latest/rest-api/#create-or-update-a-policy) of OPA for deeper understanding:

1. Register Utils module

The body of the request is simply the content of the corresponding module. In this case, it is the content of the <mark>utils.rego</mark> file

```
    PUT http://localhost:8181/v1/policies/ccmc-utils
    Content-Type: text/plain
    
    Request Body: Copy paste the content of the Utils module 
```

2. Register VC_Service module

In this case, the request body is the content of the "vc_service.rego" file

```
    PUT http://localhost:8181/v1/policies/ccmc-vcservice
    Content-Type: text/plain
    
    Request Body: Copy paste the content of the vc_service.rego file 
```

3. Register VP_Service module

In this case, the request body is the content of the "vp_service.rego" file


```
    PUT http://localhost:8181/v1/policies/ccmc-vpservice
    Content-Type: text/plain
    
    Request Body: Copy paste the content of the vp_service.rego file 
```
4. Register ABC_Checker module

In this case, the request body is the content of the "abc-checker.rego" file


```
    PUT http://localhost:8181/v1/policies/ccmc-abc-checker
    Content-Type: text/plain
    
    Request Body: Copy paste the content of the abc-checker.rego file 
```
</details>

### GET /rules/{id}
<details>
	<summary>
		<code>GET</code> 
		<code> <b>/rules/{id} </b> </code> 
		<br>
		<i> Return information about rule which is has been given in parameter </i>
	</summary>

##### Parameters
| name | type | data type | description |
| -- | -- | -- | -- |
| id| `required` | `string` | id of rule |

##### Responses
| http code | content-type | response |
| -- | -- | -- |
| `200` | `object (JSON)` | `response.json` |
```json
//response.json
{
	"id": "r1",
	"description": "desc of rule #1"
}
```
##### Example cURL
```javascript
curl -X GET http://localhost:8080/rules/r1
```
</details>

### POST /rules/check
<details>
	<summary>
		<code>POST</code> 
		<code> <b>/rules/check </b> </code> 
		<br>
		<i> Request  rule(s) checking on object given in paramaters </i>
	</summary>

##### Parameters
| name | type | data type | request |
| -- | -- | -- | -- |
| None| `required` | `object (JSON)` | `request.json` |
```json
//request.json
[
	{
		"rules": ["r1", "r2", ...]
		"vp": <json>
	}
]
```
##### Responses
| http code | content-type | response |
| -- | -- | -- |
| `200` | `application/json` | `success.json` |
```json
//success.json
{}
```
| http code | content-type | response |
| -- | -- | -- |
| `403` | `application/json` | `error.json` |
```json
//error.json
{
	"results": [
		{
			"id": "r1",
			"result": <boolean>,
			"message": "Success/Error explanation"
		},
		{
			"id": "r2",
			"result": <boolean>,
			"message": "Success/Error explanation"
		},
		...
	]
}
```
##### Example cURL
```javascript
curl -X POST -H "Content-Type: application/json" --data @request.json http://localhost:8080/rules/check
```
</details>

5. Register main module

In this case, the request body is the content of the "main.rego" file


```
    PUT http://localhost:8181/v1/policies/rules
    Content-Type: text/plain
    
    Request Body: Copy paste the content of the main.rego file 
```
### Evaluating all the implemented rules

To evaluate the rules that we have implemented in the <mark>Main</mark> module.
Below is an example of the request.

NOTE: The Body <mark>MUST</mark> contains the "input" tag which contains the VP.
1. VERIFICATION OF SINGLE RULE

With this option, the URL of the rules would be of the following format:

/v1/data/gaiax/core/abc/checker/${rule_id} 

The body of the request MUST contains the vp to be verified:

```
   POST http://localhost:8181/v1/data/gaiax/core/abc/checker/rule_01 
   Content-Type: application/json
   Request Body: Should follow the format defined in the following example
```
One example of the request body: 

```
{
    "vp": {
        "@context": [
            "https://www.w3.org/2018/credentials/v1"
        ],
        "@id": "did:web:dufourstorage.provider.dev.gaiax.ovh",
        "@type": [
            "VerifiablePresentation"
        ],
        "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
        "verifiableCredential": [
            {
                "type": [
                    "VerifiableCredential",
                    "LegalParticipant"
                ],
                "@context": [
                    "https://www.w3.org/2018/credentials/v1",
                    "https://w3id.org/security/suites/jws-2020/v1",
                    "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
                ],
                "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/8ca5cc34-7b6f-4ce1-93ef-99e1ac3d23e3/data.json",
                "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
                "issuanceDate": "2023-06-19T09:30:42Z",
                "issued": "2023-06-19T09:30:42Z",
                "validFrom": "2023-06-19T09:30:42Z",
                "expirationDate": "2023-06-21T12:17:52.960Z",
                "proof": {
                    "type": "JsonWebSignature2020",
                    "created": "2023-06-19T09:30:42Z",
                    "proofPurpose": "assertionMethod",
                    "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
                    "jws": "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJQUzI1NiJ9..a9BNfcyi1Csr0aUjb_6B7vgMSaHMZL2JWITRS96EFtivyNC2kGp-vnbC45E1QWqYsKRShYeqsA_snexLt8Z9SJLhMVhO4uxo5zvHZy1TItZcdbencFHwcJv_Mw_K7Rp8fDoO01Z6YbycZ66d-7GcA1oo-H0eiEdg7ksNPjjlbsECxyiZWoyneaQBj__rEyCnyAVg7EKaCvd-k-UcJUSdYplAtAg9PWKqEEPKaEQoXo9cRfLYJKEuppqw4t7mJmCAsARGo_0yg_wnJAl45s5ElepIZLiVkp2lLH_lhV24LpHv8J6nuYls-7hGzFZKEBzno3RLvRBaUsCUO8Aw3_YQog"
                },
                "credentialSubject": {
                    "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/provider-1/data.json",
                    "type": "gx:LegalParticipant",
                    "gx:headquarterAddress": {
                        "gx:addressCountryCode": "IT",
                        "gx:countrySubdivisionCode": "IT-25",
                        "gx:streetAddress": "Via Europa",
                        "gx:postalCode": "20121"
                    },
                    "gx:legalAddress": {
                        "gx:addressCountryCode": "IT",
                        "gx:countrySubdivisionCode": "IT-25",
                        "gx:streetAddress": "Via Europa",
                        "gx:postalCode": "20121"
                    },
                    "gx:legalRegistrationNumber": {
                        "gx:vatID": "BE0762747721"
                    }
                }
            },
            {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "type": [
                    "VerifiableCredential",
                    "ServiceOfferingCredentialExperimental"
                ],
                "id": "https://catalogue.gaia-x.eu/credentials/ServiceOfferingCredentialExperimental/1679579636460",
                "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
                "issuanceDate": "2023-03-23T13:53:56.460Z",
                "expirationDate": "2023-06-21T13:53:56.460Z",
                "credentialSubject": {
                    "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
                    "hash": "3e30bb6a868f7efa53498024092027d1799c1b435b007afbf4ab9a0b4646a595"
                },
                "proof": {
                    "type": "JsonWebSignature2020",
                    "created": "2023-03-23T13:53:56.460Z",
                    "proofPurpose": "assertionMethod",
                    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..Br8F-jZ6p55gQwzzi_r-o-rZEcuDCxE8q2mggHkAznLuoF7Tay_ySONKhVnwfLyPEPkzOxT1EAyiXTGSYsoYQ1ertWNhIFrjECuUofuoElySAYjYJ1C7rcZCKfhT9jx4npNEcJTNEfiGk_G9rdpG9qIqIzSndHAeIpdFs2SmNObLnPUzY-FMQ5aTwNZGjtveFuojln7mER4blkviV-StHidoGLwnzRKiARVnX2_mKQo2CZ0-NLHl6tAqKKvrgRZ0CugbRIP2pxQIkG08Uazj67ZdR-wcblGI89lXWBpBv1alFGYTUn6uhXyltdHt5P3-00XcPc8SGdB31s8tINOtlw",
                    "verificationMethod": "did:web:compliance.abc-federation.gaia-x.community"
                }
            },
            {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "id": "https://labelling.abc-federation.gaia-x.community/vc/8a6d70de-c995-40d8-a2af-a7bc98844ef5",
                "type": [
                    "VerifiableCredential",
                    "GrantedLabel"
                ],
                "credentialSubject": {
                    "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:3f9227f3-3ad0-447d-a875-25395453c73e/granted-label/8a6d70de-c995-40d8-a2af-a7bc98844ef5/data.json",
                    "type": [
                        "GrantedLabel"
                    ],
                    "vc": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
                    "label": {
                        "hasName": "Gaia-X label Level 1",
                        "id": "did:web:gaia-x.community:participant:020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json"
                    },
                    "proof-vc": {
                        "type": "JsonWebSignature2020",
                        "proofPurpose": "assertionMethod",
                        "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
                        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..BtcnneOWDOgKVVywlBBd4RnDh3gSn17lw53bUt4wHPVLELdkjvVj2SPSVXbkCOxuTKuO1O2nimTTkMBR5skCM_gKQscr1TX24jbPgEdrdOLa0o1_AsiilGNgDlDQvAXQI7MRCQPrHsTai5Ur8wOfeGwJDopD9t4FDMmz-wtf5RK8vJRIlaUkXqX1aPovdmdHcNsAnhFgRHt6mf0qMZfDbv0rHiTqf4G5sMtvrPeGy55HAQvGje3-A38nnGN4F46MjW9sxKXuXTJF9JML6WHCnlwRAUzgCgJ23MAsO4gEYAaQ3W-fd4RnJNUV2BWMqxV-EUbOKMy5TbUTR1tsobXt2g"
                    }
                },
                "expirationDate": "2023-06-21T00:00:00.000Z",
                "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
                "issuanceDate": "2023-03-23T13:56:02.464898+00:00",
                "proof": {
                    "type": "JsonWebSignature2020",
                    "proofPurpose": "assertionMethod",
                    "verificationMethod": "did:web:abc-federation.gaia-x.community",
                    "created": "2023-03-23T13:56:02.464898+00:00",
                    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..JAdJ7Rc2o4EUSAzHdgJmta5IC2KfOKrMO1NDDVPRLjJvkpxl7c13RpKaYLJ5RR3EZPI8JCkVEZf2ILgWOOOg4J7EDX-I9ne7R8AQGWcpEo1JRvCFTRm3FQoe6BUVhd6QVq5vQKe03PpVTp80246Kth8TyIrScsGGlj15aqa2XmyqcqaUl7rbjIqE8ve-T8SIsrQlhvEVxFlvBkbQjXME2juXefF3A1mNnPcKDiGRJme4Wwn5E91BSa7k5z33jw0DnA_-Q0i-BIsLNcAwFQCs7-t8W6_RK6urN1AA5gHH4DpTW8Ru3t7gPtHAL4aOuluDNsiEM-LQcfG4XHPiirb76w"
                }
            },
            {
                "type": [
                    "VerifiableCredential",
                    "gx:ParticipantCredential"
                ],
                "@context": [
                    "https://www.w3.org/2018/credentials/v1",
                    "https://registry.lab.gaia-x.eu//development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#",
                    "https://w3id.org/security/suites/jws-2020/v1"
                ],
                "id": "https://compliance.abc-federation.dev.gaiax.ovh/credential-offers/59676196-7638-40cf-91cd-c14212e9e312",
                "issuer": "did:web:compliance.abc-federation.dev.gaiax.ovh",
                "issuanceDate": "2023-06-20T11:54:13.499Z",
                "expirationDate": "2023-09-18T11:54:13.499Z",
                "proof": {
                    "type": "JsonWebSignature2020",
                    "created": "2023-06-20T11:54:13.525Z",
                    "proofPurpose": "assertionMethod",
                    "verificationMethod": "did:web:compliance.abc-federation.dev.gaiax.ovh",
                    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..kr3OmrWSeZdQTN8KOQ5oojVa_ud9l3bcvze4jyp0_z9A0Eut-LxMzqVJW3h-_B4ajKYKD0QKXgyPTw6y_2Bm_iRfSXBabuU7O9hOUTX7vRYD216DrpOARpwMfhgdVpoKASp8JNPl-yPiEcBqJcOI6uM_bG3r7ZKTUrslhcB-kRgX6t9Xs_vuc2mP9McKO324N87y8Sc-f-xhnq1cdIGnxz0FSTwNo9gMcdx-r293EmW2y6A7KxH1L2zZQ6FYLYzR01SCGPb4BhDExOdn2GXw8Eqn1sp4YXJS6VZxS6mKs98ynZoZrMrBAntmI0DQ7GXOzOR1OUfXLDOQAgnM2JbF7A"
                },
                "credentialSubject": {
                    "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/8ca5cc34-7b6f-4ce1-93ef-99e1ac3d23e3/data.json",
                    "hash": "dcb258d56bc03d268b685bc2cb711eef8263d9ae111a2fc48a359f4f307701ca",
                    "type": "gx:ParticipantCredential"
                }
            }
        ],
        "proof": {
            "type": "JsonWebSignature2020",
            "proofPurpose": "assertionMethod",
            "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..pnpYMYk4pGiNn-apAAlE8sPxljxwUcqT5VPf9C9vIg9eSd0I762qARrewTiQ2UEwHHkb1QSv6YJX-9KGTE_UTkHH44Q2AVKKZnU79wa_kxkzn4E8IZgBp5fMdKeMnXCNBKWMIArklXgjaLUdxncJEOF_1uun2b-A8-c1LX6oZnc4VNDF19XsHA3O6ytoTIgkOz8Uu-D2abUgI6QxeCxNauVFjyygwVSSormkXNTWQHkrMuV0tFzsJWrBA4JwG4h_0SDtKvygXm0Tyg1ZkEtKt38C1iceJIwDz3C9TaoiB6MVDzp1w5_Hq0qxr0uroPOjv6lfKJIgLrAOX5Za9d9PlA"
        }
    }
}
```



2. VERIFICATION OF A DYNAMIC SET OF RULES

With this option, you can specify the rules you want to verify in one single request. The URL dedicated to this operation is:    /v1/data/gaiax/core/abc_checker
```
   POST http://localhost:8181/v1/data/gaiax/core/abc_checker
   Content-Type: application/json
   Request Body: Should follow the format defined in the following example
```
The request body MUST contain the rules you want to verify. One example of the body request can be found below:
```   
   {
  "input": {
    "rules" : ["rule_01", "rule_02", "rule_03","rule_04"],
    "vp" : {
      "@context": [
        "https://www.w3.org/2018/credentials/v1"
      ],
      "@id": "did:web:dufourstorage.provider.dev.gaiax.ovh",
      "@type": [
        "VerifiablePresentation"
      ],
      "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
      "verifiableCredential": [
        {
          "type": [
            "VerifiableCredential",
            "LegalParticipant"
          ],
          "@context": [
            "https://www.w3.org/2018/credentials/v1",
            "https://w3id.org/security/suites/jws-2020/v1",
            "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
          ],
          "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/8ca5cc34-7b6f-4ce1-93ef-99e1ac3d23e3/data.json",
          "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
          "issuanceDate": "2023-06-19T09:30:42Z",
          "issued": "2023-06-19T09:30:42Z",
          "validFrom": "2023-06-19T09:30:42Z",
          "expirationDate": "2023-06-21T12:17:52.960Z",
          "proof": {
            "type": "JsonWebSignature2020",
            "created": "2023-06-19T09:30:42Z",
            "proofPurpose": "assertionMethod",
            "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
            "jws": "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJQUzI1NiJ9..a9BNfcyi1Csr0aUjb_6B7vgMSaHMZL2JWITRS96EFtivyNC2kGp-vnbC45E1QWqYsKRShYeqsA_snexLt8Z9SJLhMVhO4uxo5zvHZy1TItZcdbencFHwcJv_Mw_K7Rp8fDoO01Z6YbycZ66d-7GcA1oo-H0eiEdg7ksNPjjlbsECxyiZWoyneaQBj__rEyCnyAVg7EKaCvd-k-UcJUSdYplAtAg9PWKqEEPKaEQoXo9cRfLYJKEuppqw4t7mJmCAsARGo_0yg_wnJAl45s5ElepIZLiVkp2lLH_lhV24LpHv8J6nuYls-7hGzFZKEBzno3RLvRBaUsCUO8Aw3_YQog"
          },
          "credentialSubject": {
            "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/provider-1/data.json",
            "type": "gx:LegalParticipant",
            "gx:headquarterAddress": {
              "gx:addressCountryCode": "IT",
              "gx:countrySubdivisionCode": "IT-25",
              "gx:streetAddress": "Via Europa",
              "gx:postalCode": "20121"
            },
            "gx:legalAddress": {
              "gx:addressCountryCode": "IT",
              "gx:countrySubdivisionCode": "IT-25",
              "gx:streetAddress": "Via Europa",
              "gx:postalCode": "20121"
            },
            "gx:legalRegistrationNumber": {
              "gx:vatID": "BE0762747721"
            }
          }
        },
        {
          "@context": [
            "https://www.w3.org/2018/credentials/v1"
          ],
          "type": [
            "VerifiableCredential",
            "ServiceOfferingCredentialExperimental"
          ],
          "id": "https://catalogue.gaia-x.eu/credentials/ServiceOfferingCredentialExperimental/1679579636460",
          "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
          "issuanceDate": "2023-03-23T13:53:56.460Z",
          "expirationDate": "2023-06-21T13:53:56.460Z",
          "credentialSubject": {
            "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
            "hash": "3e30bb6a868f7efa53498024092027d1799c1b435b007afbf4ab9a0b4646a595"
          },
          "proof": {
            "type": "JsonWebSignature2020",
            "created": "2023-03-23T13:53:56.460Z",
            "proofPurpose": "assertionMethod",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..Br8F-jZ6p55gQwzzi_r-o-rZEcuDCxE8q2mggHkAznLuoF7Tay_ySONKhVnwfLyPEPkzOxT1EAyiXTGSYsoYQ1ertWNhIFrjECuUofuoElySAYjYJ1C7rcZCKfhT9jx4npNEcJTNEfiGk_G9rdpG9qIqIzSndHAeIpdFs2SmNObLnPUzY-FMQ5aTwNZGjtveFuojln7mER4blkviV-StHidoGLwnzRKiARVnX2_mKQo2CZ0-NLHl6tAqKKvrgRZ0CugbRIP2pxQIkG08Uazj67ZdR-wcblGI89lXWBpBv1alFGYTUn6uhXyltdHt5P3-00XcPc8SGdB31s8tINOtlw",
            "verificationMethod": "did:web:compliance.abc-federation.gaia-x.community"
          }
        },
        {
          "@context": [
            "https://www.w3.org/2018/credentials/v1"
          ],
          "id": "https://labelling.abc-federation.gaia-x.community/vc/8a6d70de-c995-40d8-a2af-a7bc98844ef5",
          "type": [
            "VerifiableCredential",
            "GrantedLabel"
          ],
          "credentialSubject": {
            "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:3f9227f3-3ad0-447d-a875-25395453c73e/granted-label/8a6d70de-c995-40d8-a2af-a7bc98844ef5/data.json",
            "type": [
              "GrantedLabel"
            ],
            "vc": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
            "label": {
              "hasName": "Gaia-X label Level 1",
              "id": "did:web:gaia-x.community:participant:020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json"
            },
            "proof-vc": {
              "type": "JsonWebSignature2020",
              "proofPurpose": "assertionMethod",
              "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
              "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..BtcnneOWDOgKVVywlBBd4RnDh3gSn17lw53bUt4wHPVLELdkjvVj2SPSVXbkCOxuTKuO1O2nimTTkMBR5skCM_gKQscr1TX24jbPgEdrdOLa0o1_AsiilGNgDlDQvAXQI7MRCQPrHsTai5Ur8wOfeGwJDopD9t4FDMmz-wtf5RK8vJRIlaUkXqX1aPovdmdHcNsAnhFgRHt6mf0qMZfDbv0rHiTqf4G5sMtvrPeGy55HAQvGje3-A38nnGN4F46MjW9sxKXuXTJF9JML6WHCnlwRAUzgCgJ23MAsO4gEYAaQ3W-fd4RnJNUV2BWMqxV-EUbOKMy5TbUTR1tsobXt2g"
            }
          },
          "expirationDate": "2023-06-21T00:00:00.000Z",
          "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
          "issuanceDate": "2023-03-23T13:56:02.464898+00:00",
          "proof": {
            "type": "JsonWebSignature2020",
            "proofPurpose": "assertionMethod",
            "verificationMethod": "did:web:abc-federation.gaia-x.community",
            "created": "2023-03-23T13:56:02.464898+00:00",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..JAdJ7Rc2o4EUSAzHdgJmta5IC2KfOKrMO1NDDVPRLjJvkpxl7c13RpKaYLJ5RR3EZPI8JCkVEZf2ILgWOOOg4J7EDX-I9ne7R8AQGWcpEo1JRvCFTRm3FQoe6BUVhd6QVq5vQKe03PpVTp80246Kth8TyIrScsGGlj15aqa2XmyqcqaUl7rbjIqE8ve-T8SIsrQlhvEVxFlvBkbQjXME2juXefF3A1mNnPcKDiGRJme4Wwn5E91BSa7k5z33jw0DnA_-Q0i-BIsLNcAwFQCs7-t8W6_RK6urN1AA5gHH4DpTW8Ru3t7gPtHAL4aOuluDNsiEM-LQcfG4XHPiirb76w"
          }
        },
        {
          "type": [
            "VerifiableCredential",
            "gx:ParticipantCredential"
          ],
          "@context": [
            "https://www.w3.org/2018/credentials/v1",
            "https://registry.lab.gaia-x.eu//development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#",
            "https://w3id.org/security/suites/jws-2020/v1"
          ],
          "id": "https://compliance.abc-federation.dev.gaiax.ovh/credential-offers/59676196-7638-40cf-91cd-c14212e9e312",
          "issuer": "did:web:compliance.abc-federation.dev.gaiax.ovh",
          "issuanceDate": "2023-06-20T11:54:13.499Z",
          "expirationDate": "2023-09-18T11:54:13.499Z",
          "proof": {
            "type": "JsonWebSignature2020",
            "created": "2023-06-20T11:54:13.525Z",
            "proofPurpose": "assertionMethod",
            "verificationMethod": "did:web:compliance.abc-federation.dev.gaiax.ovh",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..kr3OmrWSeZdQTN8KOQ5oojVa_ud9l3bcvze4jyp0_z9A0Eut-LxMzqVJW3h-_B4ajKYKD0QKXgyPTw6y_2Bm_iRfSXBabuU7O9hOUTX7vRYD216DrpOARpwMfhgdVpoKASp8JNPl-yPiEcBqJcOI6uM_bG3r7ZKTUrslhcB-kRgX6t9Xs_vuc2mP9McKO324N87y8Sc-f-xhnq1cdIGnxz0FSTwNo9gMcdx-r293EmW2y6A7KxH1L2zZQ6FYLYzR01SCGPb4BhDExOdn2GXw8Eqn1sp4YXJS6VZxS6mKs98ynZoZrMrBAntmI0DQ7GXOzOR1OUfXLDOQAgnM2JbF7A"
          },
          "credentialSubject": {
            "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/8ca5cc34-7b6f-4ce1-93ef-99e1ac3d23e3/data.json",
            "hash": "dcb258d56bc03d268b685bc2cb711eef8263d9ae111a2fc48a359f4f307701ca",
            "type": "gx:ParticipantCredential"
          }
        }
      ],
      "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..pnpYMYk4pGiNn-apAAlE8sPxljxwUcqT5VPf9C9vIg9eSd0I762qARrewTiQ2UEwHHkb1QSv6YJX-9KGTE_UTkHH44Q2AVKKZnU79wa_kxkzn4E8IZgBp5fMdKeMnXCNBKWMIArklXgjaLUdxncJEOF_1uun2b-A8-c1LX6oZnc4VNDF19XsHA3O6ytoTIgkOz8Uu-D2abUgI6QxeCxNauVFjyygwVSSormkXNTWQHkrMuV0tFzsJWrBA4JwG4h_0SDtKvygXm0Tyg1ZkEtKt38C1iceJIwDz3C9TaoiB6MVDzp1w5_Hq0qxr0uroPOjv6lfKJIgLrAOX5Za9d9PlA"
      }
    }
  }
}
   
```

An example of the response returned by ABC-Checker:

```
{
    "result": [
        {
            "description": "Verifying whether Participant Certificate has already registred in the Registry",
            "result": true,
            "rule_id": "rule_01"
        },
        {
            "description": "Verifying whether Participant is a Provider",
            "result": false,
            "rule_id": "rule_02"
        },
        {
            "description": "Verifying the signatures of all VCs/VP in the request",
            "result": [
                {
                    "is_signed": true,
                    "vc_type": "vp"
                },
                {
                    "is_signed": true,
                    "vc_type": "LegalParticipant"
                },
                {
                    "is_signed": false,
                    "vc_type": "ServiceOfferingCredentialExperimental"
                },
                {
                    "is_signed": false,
                    "vc_type": "GrantedLabel"
                },
                {
                    "is_signed": true,
                    "vc_type": "gx:ParticipantCredential"
                }
            ],
            "rule_id": "rule_03"
        },
        {
            "description": "Verifying whether all VCs belong to the same Participant",
            "result": true,
            "rule_id": "rule_04"
        }
    ]
}
```

## Run ABC-Checker with Docker
To run OPA server in docker environment, please run the command below:

```
    docker run -p 8181:8181 openpolicyagent/opa run --server
```


## Extending ABC-Checker
