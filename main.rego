package gaiax.core
import future.keywords.in 


rule_description_map = {
    "rule_01": "Verifying whether Participant Certificate has already registred in the Registry",
    "rule_02": "Verifying whether Participant is a Provider",
    "rule_03": "Verifying the signatures of all VCs/VP in the request",
    "rule_04": "Verifying whether all VCs belong to the same Participant",
    "rule_05": "Verifying whether Compliance and FC belong to the same Federation",
    "rule_06": "Verifying Object Integrity of VCs",
    "rule_07": "Verifying FederationTC",
	"rule_08":"Verifying GX Issuer",
	"rule_09":"Verifying if Participant Shape is valid",
    "rule_10":"Verifying if Participant is member of the federation",
    "rule_11":"Verifying if SO is compliant",
    "rule_12":"Verifying if vcs are signed",
    "rule_13":"Verify the existence of field ExpirationDate",
    "rule_14":"Verify the validity of the field ExpirationDate",
    "rule_15":"Verifying the existence of field CredentialStatus",
    "rule_16":"Verifying the validity of CredentialStatus",
    "rule_19":"Verifying the validity of CredentialStatus using bitstring",
    "rule_20":"Verifying the existence of field aster blockchainAddress",
    "rule_21":"Validate field aster blockchainAddress"

}

abc_checker := res {
    rules := input.rules
    res := [ obj | 
        some rule in rules
        isValid := data.gaiax.core.abc.checker[rule]
        obj := {
            "rule_id": rule,
            "result" : isValid,
            "description" : rule_description_map[rule]
        }
    ]
}

policies := res{

	opa_statuses := {
			"200" : "OK",
			"400" : "Bad Request"
	}

	res := [
		{
		"rule_01": rule_description_map["rule_01"],
		"statuses": opa_statuses
		},
		{
		"rule_02": rule_description_map["rule_02"],
		"statuses": opa_statuses
		},
		{
		"rule_03": rule_description_map["rule_03"],
		"statuses": opa_statuses
		},
		{
		"rule_04": rule_description_map["rule_04"],
		"statuses": opa_statuses
		},
		{
		"rule_05": rule_description_map["rule_05"],
		"statuses": opa_statuses
		},
		{
		"rule_06": rule_description_map["rule_06"],
		"statuses": opa_statuses
		},
		{
		"rule_07": rule_description_map["rule_07"],
		"statuses": opa_statuses
		},
		{
		"rule_08": rule_description_map["rule_08"],
		"statuses": opa_statuses
		},
		{
		"rule_09": rule_description_map["rule_09"],
		"statuses": opa_statuses
		},
        {
		"rule_10": rule_description_map["rule_10"],
		"statuses": opa_statuses
		},
        {
		"rule_11": rule_description_map["rule_11"],
		"statuses": opa_statuses
		},
        {
		"rule_12": rule_description_map["rule_12"],
		"statuses": opa_statuses
		},
        {
		"rule_13": rule_description_map["rule_13"],
		"statuses": opa_statuses
		},
        {
		"rule_14": rule_description_map["rule_14"],
		"statuses": opa_statuses
		},
        {
		"rule_15": rule_description_map["rule_15"],
		"statuses": opa_statuses
		},
        {
		"rule_16": rule_description_map["rule_16"],
		"statuses": opa_statuses
		},
        {
		"rule_19": rule_description_map["rule_19"],
		"statuses": opa_statuses
		},
        {
		"rule_20": rule_description_map["rule_20"],
		"statuses": opa_statuses
		},
        {
		"rule_21": rule_description_map["rule_21"],
		"statuses": opa_statuses
		}

	]
}