package gaiax.lib.utils

import data.gaiax.lib.utils
import future.keywords.if
import future.keywords.in

url_normalize := "https://compliance.abc-federation.dev.gaiax.ovh/api/normalize"


normalize(vcvp) := normalized_data {
    http_response := http.send({
        "method": "POST",
        "url": url_normalize,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": vcvp,
        "timeout": "20s"  #FIX THIS OR FIX COMPLIANCE
    })
    normalized_data := http_response.raw_body
}

normalize_compliance(vc) := res {
    proof_template := {
        "@context": vc["@context"],
        "type": vc["proof"]["type"],
        "created" : vc["proof"]["created"],
        "proofPurpose": vc["proof"]["proofPurpose"],
        "verificationMethod": vc["proof"]["verificationMethod"]
    }
    normalized_proof := normalize(proof_template)
    vc_template := json.remove(vc, ["proof"])
    normalized_vc := normalize(vc_template)

    hashed_normalized_proof := crypto.sha256(normalized_proof)


    hashed_normalized_vc := crypto.sha256(normalized_vc)   
  
    normalized_payload := concat("", [hashed_normalized_proof, hashed_normalized_vc]) 
    print(normalized_payload)
    payload := hex.decode(normalized_payload)
    print(payload)
    res := payload
}

get_normalized_data_without_proof(vcvp) := res {
    vcvp_without_proof := json.remove(vcvp, ["proof"])
    normalized_data := normalize(vcvp_without_proof)
    res := crypto.sha256(normalized_data)
}


get_type(vc):= res {
    vc.credentialSubject[0].type
    res := vc.credentialSubject[0].type
}

get_type(vc):= res {
    not vc.credentialSubject[0].type
    res := vc.credentialSubject.type
}

get_signer_did(vcvp) := res {
    method := vcvp["proof"]["verificationMethod"]  
    removed_key := split(method, "#")
    didParticleless := replace(removed_key[0], "did:web:","")
    didPath := replace(didParticleless, ":","/")
    finalDid := concat("",["https://",didPath])
    signer_did_url :=  concat("", [finalDid, "/.well-known/did.json"])

    #GET DID DOCUMENT of Participant => Only for self description VC and VP
    #signer_did_url := "https://dufourstorage.provider.dev.gaiax.ovh/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/did.json"   
    signer_did := fetch_data(signer_did_url)

    res := signer_did
}


is_signature_valid(vcvp) := isValid {
    payload := get_normalized_data_without_proof(vcvp)
    jws := split(vcvp["proof"]["jws"], "..")
    jwt := concat(".", [jws[0], payload, jws[1]])

    keys := {
        "keys": [key] | 
        did_doc := get_signer_did(vcvp)
        key := did_doc["verificationMethod"][_]["publicKeyJwk"]
    }
    
   isValid := io.jwt.verify_ps256(jwt, json.marshal(keys))
   #Verify the signature with the associated certificate
   #isValid := io.jwt.verify_ps256(jwt, cert)
}


is_compliance_signature_valid(vc) := isValid{
    print("verifying vcvp with proof option")
    print(vc["type"])
    payload := normalize_compliance(vc)
    curedPayload := replace(payload, ".", "")
    print(curedPayload)
    jws := split(vc["proof"]["jws"], "..")
    jwt := concat(".", [jws[0], curedPayload, jws[1]])
    keys := {
        "keys": [key] | 
        did_doc := get_signer_did(vc)
        key := did_doc["verificationMethod"][_]["publicKeyJwk"]
    }
    isValid := io.jwt.verify_ps256(jwt, json.marshal(keys))
    print("Signature verification result for proof option: ", isValid)

}

fetch_data(url) := res {
    http_response := http.send({
        "method":"GET",
        "url": url,
        "headers": {
            "Content-Type":"application/json"
        }
    })
    res := http_response.body
}

post_data(url, http_body):= res {
    http_response := http.send({
        "method":"POST",
        "url": url,
        "headers" :{
            "Content-Type":"application/json"
        },
        "body": http_body
    })

    res := http_response.body 
}

contains_fields(vcvp, field){
   comparing_arr := [field]
   #print("Comparing : ", comparing_arr)
   fields_in_vcvp := [element | vcvp[element] ]
   #print("Fields in VCVP:  ", fields_in_vcvp)
   exists := [e | 
        e := comparing_arr[_]
        e in fields_in_vcvp
   ]
   #print("Exists: ", exists)
   count(exists) > 0 

}

verify_contains_fields {
    res := contains_fields(input, "credentialStatus")

}

fetch_data_text(url) := res {

    http_response := http.send({
        "method":"GET",
        "url": url,
        "headers": {
            "Content-Type":"application/json"
        }
    })
    res := http_response.raw_body
}



