package gaiax.lib.vp.vpservice

import data.gaiax.lib.utils
import data.gaiax.lib.vc.vcservice
import future.keywords.in
import future.keywords.every


vc_participant := "LegalParticipant"
vc_participant_compliance := "gx:ParticipantCredential"


federated_calatog_federation := "did:web:compliance.abc-federation.dev.gaiax.ovh"
sign_result := [false, true]

is_vp_signed(vp) := isValid{
    is_issuer_signed := to_number(utils.is_signature_valid(vp))
	is_compliance_signed := to_number(utils.is_compliance_signature_valid(vp)) 
    is_signed := is_issuer_signed + is_compliance_signed
    isValid := sign_result[is_signed]
       
}

contains_valid_provider_vc(vp){
    vcs := vp["verifiableCredential"]
    some vc in vcs
    vc_data := vcservice.get_data_from_vc_id(vc)
    vc_data["@type"] == "gax-participant:Provider"
}

belong_to_federation(vp){
    vcs := vp["verifiableCredential"]
    compliance_domains := { domain|
        vc := vcs[_]
        not vc_participant in vc["type"]
        contains(vc["issuer"],"compliance")
        domain := vc["issuer"]
    }
    print("Compliance Domain:", compliance_domains)
    #count(compliance_domains) == 1
    #compliance_domain[0] == federated_calatog_federation
    every compliance_domain in compliance_domains{
        compliance_domain == federated_calatog_federation
    }
}

contains_vcs_of_same_provider(vp){
    vcs := vp["verifiableCredential"]
    provider_domains := { domain | 
        vc := vcs[_]
        domain := vcservice.get_participant_domain(vc)
    }
    count(provider_domains) == 1
}

contains_participant_in_registry(vp) {
    vcs := vp["verifiableCredential"]
    some vc in vcs
    vc_participant_compliance in vc["type"]
    vcservice.contains_participant_in_registry(vc)
}

get_participant_vcs(vp, participant_vc_types) := res{
    vcs := vp["verifiableCredential"]
    participant_vcs := [ vc | 
        vc := vcs[_]
        participant_vc_types[_] in vc[type]
    ]
    res := participant_vcs
}

get_participant_data_hash(vcs) := res{
    some vc in vcs
    not vc.credentialSubject.hash
    hash := vcservice.create_credential_subject_hash(vc)
    res := hash
}

verify_vcs_integrity(vp) {
    participant_vc_types := [vc_participant, vc_participant_compliance]
    
    participant_vcs := get_participant_vcs(vp, participant_vc_types)
    print(participant_vcs[0])
    print("--------------------------------")
    print(participant_vcs[1])
    participant_sd_hash := get_participant_data_hash(participant_vcs) 
    some vc in participant_vcs
    vc.credentialSubject.hash == participant_sd_hash
}