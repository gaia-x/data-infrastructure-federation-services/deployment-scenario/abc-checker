package gaiax.lib.vc.vcservice

import data.gaiax.lib.utils
import future.keywords.if
import future.keywords.in

#This is the default Registry URL. One can provide the URL via the request body as well
registry_url := "https://registry.abc-federation.dev.gaiax.ovh/api/trustAnchor/chain/file"
revocation_registry_url := "https://revocation-registry.abc-federation.dev.gaiax.ovh/api/v1/revocations/verify"


vc_participant := "LegalParticipant"
vc_participant_compliance := "gx:ParticipantCredential"

is_vc_signed(vc) := isValid{
    isValid := utils.is_signature_valid(vc)    
}

is_vc_compliance_signed(vc) := res{
    res := utils.is_compliance_signature_valid(vc)
}

get_data_from_vc_id(vc) := vc_data {
    subject_id := vc["credentialSubject"]["id"]
    subject_data_url := get_subject_data_url(subject_id)
    vc_data := utils.fetch_data(subject_data_url)
}

get_subject_data_url(subject_id) := res {
    tmp := replace(subject_id,":","/")
    tmp1 := replace(tmp, "did/web/","https://")
    res := replace(tmp1, "///","://")


}

get_subject_did_url(subject_id) := res {
    tmp := replace(subject_id,":","/")
    tmp1 := replace(tmp, "did/web/","https://")
    tmp2 := replace(tmp1, "///","://")
    res := replace(tmp2, "data.json", "did.json")


}

contains_participant_in_registry(vc){
    #subject_id := vc["credentialSubject"]["id"]
    participant_domain := get_participant_domain(vc)
    #subject_did_url := get_subject_did_url(subject_id) 
    subject_did_url := concat("",["https://",participant_domain , "/.well-known/did.json"])
    did_doc := utils.fetch_data(subject_did_url)
    certs_url := did_doc["verificationMethod"][0]["publicKeyJwk"]["x5u"]

    http_body := {
        "uri": certs_url
    }
    res := utils.post_data(registry_url, http_body)
    res[_] == true
}

get_participant_domain(vc) := domain{
    subject_id := split(vc["credentialSubject"]["id"], "participant")
    tmp := replace(subject_id[0], ":","/")
    tmp1 := replace(tmp, "/","")
    tmp2 := replace(tmp1, "https","")
    tmp3 := replace(tmp2, "didweb", "")
    domain := tmp3
}

create_credential_subject_hash(vc) := res {
    jws := vc.proof.jws
    vc_without_proof := json.remove(vc, ["proof"])
    normalized_vc_without_proof := utils.normalize(vc_without_proof)
    tbh := concat("", [normalized_vc_without_proof, jws])
    sd_hash := crypto.sha256(tbh)
    res := sd_hash
}

verify_credential_status(vc) := "NOT DEFINED" if {
    not utils.contains_fields(vc, "credentialStatus")
} else := res {
    credentialStatus := vc.credentialStatus
    statusType := credentialStatus.type

    http_body := extract_credential_verif_payload(credentialStatus)
    http_url := extract_credential_verif_url(credentialStatus)

    currentStatus := utils.post_data(http_url, http_body)
    res := validate_status(currentStatus)
}

extract_credential_verif_payload(credentialStatus) := res {
    credentialStatus.type == "StatusList2021Entry"
    res := {
        "credentialStatus" : credentialStatus
    }

} else := payload {
    credentialStatus.type == "BitstringStatusListEntry"
    payload := credentialStatus
}

extract_credential_verif_url(credentialStatus) := res1 {
    credentialStatus.type == "StatusList2021Entry"
    baseUrl := substring(credentialStatus.id, 0, indexof(credentialStatus.id, "api"))
    res1 := concat("", [baseUrl, "api/v1/revocations/verify"])

} else := res2 {
    credentialStatus.type == "BitstringStatusListEntry"
    baseUrl := substring(credentialStatus.id, 0, indexof(credentialStatus.id, "api"))
    res2 := concat("", [baseUrl, "api/v1/revocations/verify?type=BIT_STRING"])
}

validate_status(status) := false if {
    utils.contains_fields(status, "status") # Bitstring status
    contains(status["status"], "1")    # Only 0x0, 0x00, 0x000, etc are considered as valid
} else := false if {  # mean invalid
    not utils.contains_fields(status, "status") # Status List
    status[_] == true
} else := true        # Fallback to true, meaning the credential is valid


verify_credential_statusValidity(vc) := res {
    credentialStatus := vc.credentialStatus
    http_body := {
        "credentialStatus" : credentialStatus
    }
    status = utils.post_data(revocation_registry_url, http_body)
    res:= status["suspended"] == status["revoked"]
}


verify_credential_status_existence(vc) := false if {
    not utils.contains_fields(vc, "credentialStatus")
} else := true



verifyExpirationDate(vc):= false if {
         time.now_ns() > time.parse_rfc3339_ns(vc.expirationDate)
} else := true

verifyExpirationDateExistence(vc):= false if {
        not vc.expirationDate 
} else := true if {
        time.now_ns() > time.parse_rfc3339_ns(vc.expirationDate)
        } else := true
    

check_tc_validity(vc, url) := res {
    tc := vc.credentialSubject["aster-conformity:termsAndConditions"]
    print(tc)
    federationTC := utils.fetch_data_text(url)
    print(federationTC)
    isValid := tc==federationTC
    res:= isValid
}

check_trusted_issuer(vc, url) := res {
    issuer := vc.issuer
    splited_issuer:= replace(issuer, "did:web:","")
    trusted_issuer := utils.fetch_data(url)
    some elem in trusted_issuer
    elem == splited_issuer
    res := elem
}




check_matching_schema(vc, url) := res {
    
   schema := utils.fetch_data(url)
   schemacheck := json.verify_schema(schema)
    elem:= json.match_schema(vc,schema)
    res := elem[0] == true
}

check_trusted_issuer_service_offering(vc, url) := res {
    issuer := vc.proof.verificationMethod
    removed_key = split(issuer, "#")
    splited_issuer:= replace(removed_key[0], "did:web:","")
    trusted_issuer := utils.fetch_data(url)
    some elem in trusted_issuer
    elem == splited_issuer
    res := elem
}


check_Federation_Participant(vc, url) := res {
    issuer := vc.proof.verificationMethod
    federation:= vc.credentialSubject["vcard:title"]
    federation=="Aster-X Membership"
    removed_key = split(issuer, "#")
    splited_issuer:= replace(removed_key[0], "did:web:","")
    print(splited_issuer)
    trusted_issuer := utils.fetch_data(url)
    some elem in trusted_issuer
    elem == splited_issuer
    res := elem
}

check_SO_Trusted(vc, url) := res {
    issuer := vc.proof.verificationMethod
    removed_key = split(issuer, "#")
    splited_issuer:= replace(removed_key[0], "did:web:","")
    trusted_issuer := utils.fetch_data(url)
    some elem in trusted_issuer
    elem == splited_issuer
    some cred in vc.credentialSubject
    cred["gx:type"]="gx:ServiceOffering"
    res := elem

}

verify_aster_business_field(vc) := false if {
    not utils.contains_fields(vc.credentialSubject, "aster-conformity:blockchainAddress")
} else := true


verify_obelX_business_field(vc) := false if {
    not utils.contains_fields(vc.credentialSubject, "obel-conformity:referenceNumber")
} else := true

validate_aster_business_field(vc) := res {
    blockchainAddress := vc.credentialSubject["aster-conformity:blockchainAddress"]
    print(blockchainAddress)
    check := startswith(blockchainAddress, "0x")
    print(check)
    res := check
}


validate_obelX_business_field(vc) := false if {
    not utils.contains_fields(vc, "obel-conformity:referenceNumber")
} else := true

#test_verify_credential_status {
#    res := verify_credential_status(input)
#    print("res: ", res)
#} 
